import MyBigNumber
import logging

logging.basicConfig(level=logging.INFO)


def user_input() -> tuple:
    """
    Receives input integers from user.

    :return: A tuple of strings.
    """

    while True:
        try:
            # receives the input from user.
            stn1 = int(input("Enter the first number: "))
            stn2 = int(input("Enter the second number: "))
            break
        # if the input is not an integer, the program will ask the user to enter a valid integer.
        except ValueError:
            logging.error("Invalid input. Please enter a valid integer.", exc_info=True)

    # converts the integers to strings and returns them.
    tuple_of_integers = (str(stn1), str(stn2))
    return tuple_of_integers


def main():
    stn1, stn2 = user_input()

    # prints the sum of the integers.
    logging.info(f"The sum of the two strings is: {MyBigNumber.MyBigNumber.sum(stn1, stn2)}".format())


if __name__ == "__main__":
    main()