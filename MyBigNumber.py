class MyBigNumber:
    def __init__(self, stn1: str, stn2: str):
        # Store the strings in the class.
        self.stn1 = stn1
        self.stn2 = stn2


    def sum(stn1: str, stn2: str) -> str:
        """
        Add two strings of integers, digit by digit, from right to left.

        :param stn1: The first string.
        :param stn2: The second string.
        :return: The sum of the two strings in string format.
        """

        # Initialize the carry and the result.
        carry = 0
        result = ""

        # Finds the larger string, then traverse the strings from right to left.
        for i in range(max(len(stn1), len(stn2))):
            # Get the digit at the current index.
            digit1 = int(stn1[-(i + 1)]) if i < len(stn1) else 0
            digit2 = int(stn2[-(i + 1)]) if i < len(stn2) else 0

            # Add the digits and the carry.
            digit_sum = digit1 + digit2 + carry

            # Update the carry.
            carry = digit_sum // 10

            # Update the result.
            result = str(digit_sum % 10) + result

        # If there is a carry left, add it to the result.
        if carry > 0:
            result = str(carry) + result

        return result

