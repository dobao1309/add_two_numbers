import MyBigNumber
import unittest


class TestBigNumber(unittest.TestCase):
    def test_sum1(self):
        actual = MyBigNumber.MyBigNumber.sum("7490840301", "4729812955")
        expected = "12220653256"
        self.assertEqual(actual, expected)

    def test_sum_2(self):
        actual = MyBigNumber.MyBigNumber.sum("1", "2")
        expected = "3"
        self.assertEqual(actual, expected)

    def test_sum_3(self):
        actual = MyBigNumber.MyBigNumber.sum("99999999999999", "9999999999999999999999999999999")
        expected = "10000000000000000099999999999998"
        self.assertEqual(actual, expected)

    # float numbers test case.
    def test_float_input_exception(self):
        with self.assertRaises(ValueError) as exception_context:
            MyBigNumber.MyBigNumber.sum("7.5", "9.3")
        self.assertEqual(str(exception_context.exception), "invalid literal for int() with base 10: '.'")


    # negative numbers test case.
    def test_negative_input_exception(self):
        with self.assertRaises(ValueError) as exception_context:
            MyBigNumber.MyBigNumber.sum("-1", "-940")
        self.assertEqual(str(exception_context.exception), "invalid literal for int() with base 10: '-'")

    # not a number test case.
    def test_invalid_input_exception(self):
        with self.assertRaises(ValueError) as exception_context:
            MyBigNumber.MyBigNumber.sum("abcd", "efghi")
        self.assertEqual(str(exception_context.exception), "invalid literal for int() with base 10: 'd'")


if __name__ == "__main__":
    unittest.main()
