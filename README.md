File MyBigNumber chứa 1 lớp cùng tên với nó, nó thực hiện tính toán 2 cộng số nguyên dương lớn (được biểu diễn dưới dạng chuỗi). Thuật toán này thực hiện tính toán bằng cách cộng từng số hạng.

Hàm __init__: dùng để khởi tạo lớp
Hàm sum: dùng cộng 2 số hạng với nhau. Trong đó, stn1 là số hạng thứ nhất, stn2 là số hạng thứ hai. Cả hai được biểu diễn dưới dạng chuỗi.

File Program: để chạy chương trình trên.
Hàm user_input: nhận số người dùng nhập và trả về 1 danh sách dưới dạng tuple.
Hàm main: thực hiện hàm user_input để có được 2 số người dùng cần cộng, sau đó thực hiện tính toán.

File UnitTest: chứa các unit test cho chương trình trên.